# Soal 1 - Membuat Database
create database myshop;
use myshop;


# Soal 2 - Membuat Table di dalam Database
- Table Users
create table users (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));


- Table Categories
create table categories (
    -> id int auto_increment primary key,
    -> name varchar(255));


- Table Items
 create table items (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key (category_id) references categories(id));


# Soal 3 - Memasukkan Data pada Table
- Table Users
insert into users values
    -> ('', 'John Doe', 'john@doe.com', 'john123'),
    -> ('', 'Jane Doe', 'jane@doe.com', 'jenita123');


- Table Categories
insert into categories values
    -> ('', 'gadget'),
    -> ('', 'cloth'),
    -> ('', 'men'),
    -> ('', 'women'),
    -> ('', 'branded');


- Table Items
insert into items values
    -> ('', 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'),
    -> ('', 'Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'),
    -> ('', 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');


# Soal 4 - Mengambil Data dari Database
a. Mengambil Data Users
select id, name from users;

b. Mengambil Data Items
- data item pada table items yang memiliki harga di atas 1000000
select * from items where price > 1000000;

- data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja)
select * from items where name like '%uniklooh%';


c. Menampilkan Data Items Join dengan Kategori 
select a.name, a.description, a.price, a.stock, a.category_id, b.name as kategori from items a inner join categories b on(a.category_id=b.id);


# Soal 5 - Mengubah Data dari Database
update items set price = 2500000 where id = 1;



