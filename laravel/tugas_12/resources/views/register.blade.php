<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
</head>

<body>

    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <div>
            <label>First Name:</label><br><br>
            <input type="text" name="first-name"><br><br>
        </div>
        <div>
            <label>Last Name:</label><br><br>
            <input type="text" name="last-name"><br><br>
        </div>
        <div>
            <label>Gender:</label><br><br>
            <input type="radio" id="male" name="male" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="fav_language" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="fav_language" value="Other">
            <label for="other">Other</label><br><br>
        </div>
        <div>
            <label>Nationality:</label><br><br>
            <select>
                <option value="">--Choose Nationality--</option>
                <option value="Indonesian">Indonesian</option>
                <option value="South Korea">South Korea</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Thailand">Thailand</option>
                <option value="Amerika">Amerika</option>
            </select>
            <br><br>
        </div>
        <div>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" id="bahasa" name=" bahasa" value="Bahasa Indonesia">
            <label for="bahasa">Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" name=" english" value="English">
            <label for="english">English</label><br>
            <input type="checkbox" id="other" name=" other" value="Other">
            <label for="other">Other</label><br><br>
        </div>
        <div>
            <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        </div>
        <div>
            <input type="submit" value="Sign Up">
        </div>

    </form>

</body>

</html>