@extends('templates.main')

@section('judul')
    Tambah Cast
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="nama" name="nama">
                @error('nama')
                    <p class="text-danger">*{{ $message }}</p>
                @enderror
            </div>
        </div>
        
        <div class="form-group row">
            <label for="umur" class="col-sm-2 col-form-label">Umur</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="umur" name="umur">
                @error('umur')
                    <p class="text-danger">*{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="bio" class="col-sm-2 col-form-label">Bio</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="bio" name="bio">
                @error('bio')
                    <p class="text-danger">*{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-2"></label>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="/cast" class="btn btn-danger ml-2">Kembali</a>
            </div>
        </div>
    </form>
@endsection