@extends('templates.main')

@section('judul')
    Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary mb-4">Tambah</a>
    
    <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            {{-- <th scope="col">Umur</th>
            <th scope="col">Bio</th> --}}
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $value->nama }}</td>
                    <td>
                        <form action="/cast/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            
                            <a href="/cast/{{ $value->id }}" class="btn btn-success">Detail</a>
                            <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning">Edit</a>
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection