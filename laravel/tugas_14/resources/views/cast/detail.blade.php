@extends('templates.main')

@section('judul')
    Detail Cast
@endsection

@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">{{ $cast->nama}}</h4> <br>
          <span class="badge badge-info">{{ $cast->umur }}</span>
          <p class="card-text">{{ $cast->bio }}</p>
          <a href="/cast" class="btn btn-danger btn-sm">Kembali</a>
        </div>
      </div>
    </div>
  </div>
@endsection